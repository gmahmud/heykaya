<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Heykaya
 */


function heykaya_register_main_options_metabox() {
	$prefix = 'heykaya_';
	/**
	 * =====================================================================
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * 						Header section
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * =====================================================================
	 */
	$args = array(
		'id'           => $prefix.'main_options_page',
		'title'        => 'Heykaya Settings',
		'object_types' => array( 'options-page' ),
		'option_key'   => $prefix.'main_options',
		'tab_group'    => $prefix.'main_options',
		'tab_title'    => 'Heykaya Settings',
	);

	// 'tab_group' property is supported in > 2.4.0.
	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
		$args['display_cb'] = $prefix.'options_display_with_tabs';
	}

	$main_options = new_cmb2_box( $args );

	$main_options->add_field( array(
		'name'    => 'Hero Image',
		'desc'    => 'Upload header background image',
		'id'      => 'hero_image',
		'type'    => 'file',
		// 'repeatable' => true,
		// 'sortable'=> true,
	) );

	$main_options->add_field( array(
		'name'    => 'Big Logo',
		'desc'    => 'Upload the logo',
		'id'      => 'big_logo',
		'type'    => 'file',
		// 'repeatable' => true,
		// 'sortable'=> true,
	) );

	$main_options->add_field( array(
		'name'    => 'Arabic Logo',
		'desc'    => 'Upload the Arabic logo',
		'id'      => 'big_logo_ar',
		'type'    => 'file',
		// 'repeatable' => true,
		// 'sortable'=> true,
	) );

	
	/**
	 * =====================================================================
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * 						Features section
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * =====================================================================
	 */
	$args = array(
		'id'           => $prefix.'features_options_page',
		'menu_title'   => 'Features Section', // Use menu title, & not title to hide main h2.
		'object_types' => array( 'options-page' ),
		'option_key'   => $prefix.'features_section',
		'parent_slug'  => $prefix.'main_options',
		'tab_group'    => $prefix.'main_options',
		'tab_title'    => 'Features Section',
	);

	// 'tab_group' property is supported in > 2.4.0.
	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
		$args['display_cb'] = $prefix.'options_display_with_tabs';
	}

	$features_section_options = new_cmb2_box( $args );

	$features_section_options->add_field( array(
		'name'    => 'Section Header',
		'desc'    => 'Features section title header',
		'id'      => 'features_title',
		'type'    => 'text',
		'default' => 'THE PIONEERS OF REALITIES!',
	));

	$features_section_options->add_field( array(
		'name'    => 'Section Description',
		'desc'    => 'Features section description',
		'id'      => 'features_desc',
		'type'    => 'textarea',
	));

	$features_section_options->add_field( array(
		'name'    => 'Second Header',
		'desc'    => 'Features section second title header',
		'id'      => 'features_title2',
		'type'    => 'text',
		'default' => 'OUR CREATIONS',
	));

	$game_group_id = $features_section_options->add_field( array(
		'id'          => 'game_group',
		'type'        => 'group',
		'repeatable'  => true,
		'options'     => array(
			'group_title'   => 'Game {#}',
			'add_button'    => 'Add Another Game',
			'remove_button' => 'Remove Game',
			'closed'        => true,  // Repeater fields closed by default - neat & compact.
			'sortable'      => true,  // Allow changing the order of repeated groups.
		),
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Progress Status',
		'id'   => 'progress_status',
		'type'    => 'radio_inline',
		'options' => array(
			'in_progress' => __( 'In Progress', 'cmb2' ),
			'complete'   => __( 'Completed', 'cmb2' ),
		),
		'default' => 'in_progress',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Thumbnail',
		'desc' => 'Game thumbnail to on the home page',
		'id'   => 'game_thumb',
		'type' => 'file',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Title',
		'desc' => 'Name of the game',
		'id'   => 'game_title',
		'type' => 'text',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Description',
		'desc' => 'Full description about the game',
		'id'   => 'game_desc',
		'type' => 'textarea',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Artwork',
		'desc' => 'Game artwork cover for the popup',
		'id'   => 'game_popup_art',
		'type' => 'file',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Site URL',
		'desc' => 'Game URL for the popup',
		'id'   => 'game_popup_url',
		'type' => 'text_url',
		'default' => '#',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Support URL',
		'desc' => 'Support URL for the popup',
		'id'   => 'game_popup_support',
		'type' => 'text_url',
		'default' => '#',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game FB Page',
		'desc' => 'FB page URL for the icon for the popup',
		'id'   => 'game_popup_fb',
		'type' => 'text_url',
		'default' => '#',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Twitter Page',
		'desc' => 'Twitter page URL for the icon for the popup',
		'id'   => 'game_popup_twitter',
		'type' => 'text_url',
		'default' => '#',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Instagram Page',
		'desc' => 'Instagram page URL for the icon for the popup',
		'id'   => 'game_popup_insta',
		'type' => 'text_url',
		'default' => '#',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Youtube Channel',
		'desc' => 'Youtube Channel URL for the icon for the popup',
		'id'   => 'game_popup_youtube',
		'type' => 'text_url',
		'default' => '#',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Game Twitch Channel',
		'desc' => 'Twitch Channel URL for the icon for the popup',
		'id'   => 'game_popup_twitch',
		'type' => 'text_url',
		'default' => '#',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Youtube/Vimeo Video for the Slider',
		'desc' => 'For the slider in game popup',
		'id'   => 'game_popup_slider_video',
		'type' => 'text_url',
		'repeatable'  => true,
	));
	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Upload images as slider items',
		'desc' => 'For the slider in game popup',
		'id'   => 'game_popup_slider_images',
		'type' => 'file_list',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Visit URL',
		'desc' => 'Big button at the bottom of the popup',
		'id'   => 'game_popup_visit_url',
		'type' => 'text_url',
		'default' => '#',
	));

	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Apple Store URL',
		'desc' => 'To be put in the store popup game list',
		'id'   => 'game_store_url_apple',
		'type' => 'text_url',
	));
	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Play Store URL',
		'desc' => 'To be put in the store popup game list',
		'id'   => 'game_store_url_play',
		'type' => 'text_url',
	));
	$features_section_options->add_group_field( $game_group_id, array(
		'name' => 'Steam Store URL',
		'desc' => 'To be put in the store popup game list',
		'id'   => 'game_store_url_steam',
		'type' => 'text_url',
	));

	/**
	 * =====================================================================
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * 						Multimedia section
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * =====================================================================
	 */
	$args = array(
		'id'           => $prefix.'gallery_options_page',
		'menu_title'   => 'Multimedia Section', // Use menu title, & not title to hide main h2.
		'object_types' => array( 'options-page' ),
		'option_key'   => $prefix.'gallery_section',
		'parent_slug'  => $prefix.'main_options',
		'tab_group'    => $prefix.'main_options',
		'tab_title'    => 'Multimedia Section',
	);

	// 'tab_group' property is supported in > 2.4.0.
	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
		$args['display_cb'] = $prefix.'options_display_with_tabs';
	}

	$gallery_section_options = new_cmb2_box( $args );

	$gallery_section_options->add_field( array(
		'name'    => 'Section Header',
		'desc'    => 'Gallery section title header',
		'id'      => 'gallery_title',
		'type'    => 'text',
		'default' => 'JOIN IN ON THE JOURNEY',
	));

	$gallery_section_options->add_field( array(
		'name'    => 'Section Description',
		'desc'    => 'Gallery section description',
		'id'      => 'gallery_desc',
		'type'    => 'textarea',
	));

	$gallery_slider_id = $gallery_section_options->add_field( array(
		'id'          => 'gallery_item_group',
		'type'        => 'group',
		'repeatable'  => true,
		'options'     => array(
			'group_title'   => 'Slider Item {#}',
			'add_button'    => 'Add Another Slider Item',
			'remove_button' => 'Remove Slider Item',
			'closed'        => true,  // Repeater fields closed by default - neat & compact.
			'sortable'      => true,  // Allow changing the order of repeated groups.
		),
	));

	$gallery_section_options->add_group_field( $gallery_slider_id, array(
		'name' => 'Enable Video',
		'id'   => 'vid_yes',
		'type'    => 'radio_inline',
		'options' => array(
			'yes' => __( 'Yes', 'cmb2' ),
			'no'   => __( 'No', 'cmb2' ),
		),
		'default' => 'no',
	));

	$gallery_section_options->add_group_field( $gallery_slider_id, array(
		'name' => 'Active or Not?',
		'desc' => 'Choose this for only one item. Selecting this option to active, will make it display first.',
		'id'   => 'active_status',
		'type'    => 'radio_inline',
		'options' => array(
			'active' => __( 'Active', 'cmb2' ),
			'notactive'   => __( 'Not Active', 'cmb2' ),
		),
		'default' => 'notactive',
	));

	$gallery_section_options->add_group_field( $gallery_slider_id, array(
		'name' => 'Image',
		'desc' => 'Slider Image',
		'id'   => 'gallery_slider_img',
		'type' => 'file',
	));

	$gallery_section_options->add_group_field( $gallery_slider_id, array(
		'name' => 'Video URL',
		'desc' => 'Youtube only (note: For non video items, keep it blank)',
		'id'   => 'gallery_slider_vid',
		'type' => 'text_url',
	));

	$gallery_section_options->add_group_field( $gallery_slider_id, array(
		'name' => 'Caption',
		'desc' => 'Applicable for non-video items',
		'id'   => 'gallery_slider_caption',
		'type' => 'text',
	));

	/**
	 * =====================================================================
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * 						Store section
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * =====================================================================
	 */

	$args = array(
		'id'           => $prefix.'store_options_page',
		'menu_title'   => 'Store Section', // Use menu title, & not title to hide main h2.
		'object_types' => array( 'options-page' ),
		'option_key'   => $prefix.'store_section',
		'parent_slug'  => $prefix.'main_options',
		'tab_group'    => $prefix.'main_options',
		'tab_title'    => 'Store Section',
	);

	// 'tab_group' property is supported in > 2.4.0.
	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
		$args['display_cb'] = $prefix.'options_display_with_tabs';
	}

	$store_section_options = new_cmb2_box( $args );

	$store_section_options->add_field( array(
		'name'    => 'Section Header',
		'desc'    => 'Store section title header',
		'id'      => 'store_title',
		'type'    => 'text',
		'default' => 'CHOOSE YOUR WEAPON',
	));

	$store_section_options->add_field( array(
		'name'    => 'Section Description',
		'desc'    => 'Store section description',
		'id'      => 'store_desc',
		'type'    => 'textarea',
	));

	// APPLE STORE
	$apple_store_id = $store_section_options->add_field( array(
		'id'          => 'apple_store_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'Apple Store',
			'closed'        => true,
		),
	));

	$store_section_options->add_group_field( $apple_store_id, array(
		'name' => 'Card Image',
		'desc' => 'Upload Image',
		'id'   => 'apple_store_card_img',
		'type' => 'file',
	));

	$store_section_options->add_group_field( $apple_store_id, array(
		'name' => 'Card Description',
		'desc' => 'Full Description',
		'id'   => 'apple_store_card_desc',
		'type' => 'textarea',
	));

	$store_section_options->add_group_field( $apple_store_id, array(
		'name' => 'Store External URL',
		'id'   => 'apple_store_url',
		'type' => 'text_url',
	));

	// Play STORE
	$play_store_id = $store_section_options->add_field( array(
		'id'          => 'play_store_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'Play Store',
			'closed'        => true,
		),
	));

	$store_section_options->add_group_field( $play_store_id, array(
		'name' => 'Card Image',
		'desc' => 'Upload Image',
		'id'   => 'play_store_card_img',
		'type' => 'file',
	));

	$store_section_options->add_group_field( $play_store_id, array(
		'name' => 'Card Description',
		'desc' => 'Full Description',
		'id'   => 'play_store_card_desc',
		'type' => 'textarea',
	));

	$store_section_options->add_group_field( $play_store_id, array(
		'name' => 'Store External URL',
		'id'   => 'play_store_url',
		'type' => 'text_url',
	));

	// Steam STORE
	$steam_store_id = $store_section_options->add_field( array(
		'id'          => 'steam_store_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'Steam Store',
			'closed'        => true,
		),
	));

	$store_section_options->add_group_field( $steam_store_id, array(
		'name' => 'Card Image',
		'desc' => 'Upload Image',
		'id'   => 'steam_store_card_img',
		'type' => 'file',
	));

	$store_section_options->add_group_field( $steam_store_id, array(
		'name' => 'Card Description',
		'desc' => 'Full Description',
		'id'   => 'steam_store_card_desc',
		'type' => 'textarea',
	));

	$store_section_options->add_group_field( $steam_store_id, array(
		'name' => 'Store External URL',
		'id'   => 'steam_store_url',
		'type' => 'text_url',
	));

	// Heykaya STORE
	$heykaya_store_id = $store_section_options->add_field( array(
		'id'          => 'heykaya_store_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'Heykaya Store',
			'closed'        => true,
		),
	));

	$store_section_options->add_group_field( $heykaya_store_id, array(
		'name' => 'Card Image',
		'desc' => 'Upload Image',
		'id'   => 'heykaya_store_card_img',
		'type' => 'file',
	));

	$store_section_options->add_group_field( $heykaya_store_id, array(
		'name' => 'Card Description',
		'desc' => 'Full Description',
		'id'   => 'heykaya_store_card_desc',
		'type' => 'textarea',
	));

	$store_section_options->add_group_field( $heykaya_store_id, array(
		'name' => 'Store External URL',
		'id'   => 'heykaya_store_url',
		'type' => 'text_url',
	));

	$store_section_options->add_group_field( $heykaya_store_id, array(
		'name' => 'Store External URL',
		'id'   => 'heykaya_store_url',
		'type' => 'text_url',
	));

	$heykaya_scroll_items_id = $store_section_options->add_field( array(
		'id'          => 'heykaya_store_popup_scroll_items',
		'type'        => 'group',
		'repeatable'  => true,
		'options'     => array(
			'group_title'   => 'Heykaya Store popup Goods Item {#}',
			'add_button'    => 'Add Another Goods Item',
			'remove_button' => 'Remove Goods Item',
			'closed'        => true,  // Repeater fields closed by default - neat & compact.
			'sortable'      => true,  // Allow changing the order of repeated groups.
		),
	));

	$store_section_options->add_group_field( $heykaya_scroll_items_id, array(
		'name' => 'Goods Image',
		'desc' => 'Upload Image',
		'id'   => 'heykaya_store_goods_img',
		'type' => 'file',
	));

	$store_section_options->add_group_field( $heykaya_scroll_items_id, array(
		'name' => 'Goods Title',
		'desc' => 'Name of the goods item',
		'id'   => 'heykaya_store_goods_title',
		'type' => 'text',
	));

	$store_section_options->add_group_field( $heykaya_scroll_items_id, array(
		'name' => 'Goods Description',
		'desc' => 'Full Description',
		'id'   => 'heykaya_store_goods_desc',
		'type' => 'textarea',
		'default' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut',
	));

	$store_section_options->add_group_field( $heykaya_scroll_items_id, array(
		'name' => 'Goods URL',
		'desc' => 'URL of the goods item',
		'id'   => 'heykaya_store_goods_url',
		'type' => 'text_url',
		'default' => '#',
	));


	/**
	 * =====================================================================
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * 						Team section
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * =====================================================================
	 */

	$args = array(
		'id'           => $prefix.'team_options_page',
		'menu_title'   => 'Team Section', // Use menu title, & not title to hide main h2.
		'object_types' => array( 'options-page' ),
		'option_key'   => $prefix.'team_section',
		'parent_slug'  => $prefix.'main_options',
		'tab_group'    => $prefix.'main_options',
		'tab_title'    => 'Team Section',
	);

	// 'tab_group' property is supported in > 2.4.0.
	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
		$args['display_cb'] = $prefix.'options_display_with_tabs';
	}

	$team_section_options = new_cmb2_box( $args );

	$team_section_options->add_field( array(
		'name'    => 'Section Header',
		'desc'    => 'Team section title header',
		'id'      => 'team_title',
		'type'    => 'text',
		'default' => 'THE CONCLAVE OF CREATORS',
	));

	$team_section_options->add_field( array(
		'name'    => 'Section Description',
		'desc'    => 'Team section description',
		'id'      => 'team_desc',
		'type'    => 'textarea',
	));

	$team_slider_id = $team_section_options->add_field( array(
		'id'          => 'team_member_group',
		'type'        => 'group',
		'repeatable'  => true,
		'options'     => array(
			'group_title'   => 'Team Member {#}',
			'add_button'    => 'Add Another Team Member',
			'remove_button' => 'Remove Team Member',
			'closed'        => true,  // Repeater fields closed by default - neat & compact.
			'sortable'      => true,  // Allow changing the order of repeated groups.
		),
	));

	$team_section_options->add_group_field( $team_slider_id, array(
		'name' => 'Name',
		'desc' => 'Member Name',
		'id'   => 'team_member_name',
		'type' => 'text',
	));

	$team_section_options->add_group_field( $team_slider_id, array(
		'name' => 'Designation',
		'desc' => 'Member Designation',
		'id'   => 'team_member_designation',
		'type' => 'text',
	));

	$team_section_options->add_group_field( $team_slider_id, array(
		'name' => 'Image',
		'desc' => 'Member image',
		'id'   => 'team_member_image',
		'type' => 'file',
	));

	$team_section_options->add_group_field( $team_slider_id, array(
		'name' => 'Bio',
		'desc' => 'About Member',
		'id'   => 'team_member_bio',
		'type' => 'textarea',
	));

	$team_section_options->add_group_field( $team_slider_id, array(
		'name' => 'Facebook',
		'desc' => 'Facebook URL',
		'id'   => 'team_member_fb',
		'type' => 'text_url',
		'default' => '#',
	));

	$team_section_options->add_group_field( $team_slider_id, array(
		'name' => 'Twitter',
		'desc' => 'Twitter URL',
		'id'   => 'team_member_twitter',
		'type' => 'text_url',
		'default' => '#',
	));

	$team_section_options->add_group_field( $team_slider_id, array(
		'name' => 'LinkedIn',
		'desc' => 'LinkedIn URL',
		'id'   => 'team_member_linkedin',
		'type' => 'text_url',
		'default' => '#',
	));

	/**
	 * =====================================================================
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * 						Social section
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * =====================================================================
	 */
	
	$args = array(
		'id'           => $prefix.'social_options_page',
		'menu_title'   => 'Social Section', // Use menu title, & not title to hide main h2.
		'object_types' => array( 'options-page' ),
		'option_key'   => $prefix.'social_section',
		'parent_slug'  => $prefix.'main_options',
		'tab_group'    => $prefix.'main_options',
		'tab_title'    => 'Social Section',
	);

	// 'tab_group' property is supported in > 2.4.0.
	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
		$args['display_cb'] = $prefix.'options_display_with_tabs';
	}

	$social_section_options = new_cmb2_box( $args );

	$social_section_options->add_field( array(
		'name'    => 'Section Header',
		'desc'    => 'Social section title header',
		'id'      => 'social_title',
		'type'    => 'text',
		'default' => 'TALK WITH US',
	));

	$social_section_options->add_field( array(
		'name'    => 'Section Description',
		'desc'    => 'Social section description',
		'id'      => 'social_desc',
		'type'    => 'textarea',
	));

	// Blog
	$blog_id = $social_section_options->add_field( array(
		'id'          => 'blog_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'Development Blog',
			'closed'        => true,
		),
	));

	$social_section_options->add_group_field( $blog_id, array(
		'name' => 'Card Image',
		'desc' => 'Upload Image',
		'id'   => 'social_blog_card_img',
		'type' => 'file',
	));

	$social_section_options->add_group_field( $blog_id, array(
		'name' => 'Card Description',
		'desc' => 'Full Description',
		'id'   => 'social_blog_card_desc',
		'type' => 'textarea',
	));

	$social_section_options->add_group_field( $blog_id, array(
		'name' => 'Blog External URL', //for landing page section
		'id'   => 'social_blog_url',
		'type' => 'text_url',
	));

	// blog popup scroll
	$blog_scroll_items_id = $social_section_options->add_field( array(
		'id'          => 'blog_popup_scroll_items',
		'type'        => 'group',
		'repeatable'  => true,
		'options'     => array(
			'group_title'   => 'Blog Item {#}',
			'add_button'    => 'Add Another Blog Item',
			'remove_button' => 'Remove Blog Item',
			'closed'        => true,  // Repeater fields closed by default - neat & compact.
			'sortable'      => true,  // Allow changing the order of repeated groups.
		),
	));

	$social_section_options->add_group_field( $blog_scroll_items_id, array(
		'name' => 'Blog Image',
		'desc' => 'Upload Image',
		'id'   => 'blog_item_img',
		'type' => 'file',
	));

	$social_section_options->add_group_field( $blog_scroll_items_id, array(
		'name' => 'Blog Title',
		'desc' => 'Name of the Blog Item',
		'id'   => 'blog_item_title',
		'type' => 'text',
		'default' => 'Dev Blog Title'
	));

	$social_section_options->add_group_field( $blog_scroll_items_id, array(
		'name' => 'Blog Description',
		'desc' => 'Full Description',
		'id'   => 'blog_item_desc',
		'type' => 'textarea',
		'default' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut',
	));

	$social_section_options->add_group_field( $blog_scroll_items_id, array(
		'name' => 'Blog Item URL',
		'desc' => 'URL of the blog item',
		'id'   => 'blog_item_url',
		'type' => 'text_url',
		'default' => '#',
	));

	// Community
	$community_id = $social_section_options->add_field( array(
		'id'          => 'community_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'Our Community',
			'closed'        => true,
		),
	));

	$social_section_options->add_group_field( $community_id, array(
		'name' => 'Card Image',
		'desc' => 'Upload Image',
		'id'   => 'social_community_card_img',
		'type' => 'file',
	));

	$social_section_options->add_group_field( $community_id, array(
		'name' => 'Card Description',
		'desc' => 'Full Description',
		'id'   => 'social_community_card_desc',
		'type' => 'textarea',
	));

	$social_section_options->add_group_field( $community_id, array(
		'name' => 'Community External URL',
		'id'   => 'social_community_url',
		'type' => 'text_url',
	));

	// community popup scroll items
	$community_scroll_items_id = $social_section_options->add_field( array(
		'id'          => 'community_popup_scroll_items',
		'type'        => 'group',
		'repeatable'  => true,
		'options'     => array(
			'group_title'   => 'Community Item {#}',
			'add_button'    => 'Add Another Community Item',
			'remove_button' => 'Remove Community Item',
			'closed'        => true,  // Repeater fields closed by default - neat & compact.
			'sortable'      => true,  // Allow changing the order of repeated groups.
		),
	));

	$social_section_options->add_group_field( $community_scroll_items_id, array(
		'name' => 'Community Image',
		'desc' => 'Upload Image',
		'id'   => 'community_item_img',
		'type' => 'file',
	));

	$social_section_options->add_group_field( $community_scroll_items_id, array(
		'name' => 'Community Title',
		'desc' => 'Name of the Community Item',
		'id'   => 'community_item_title',
		'type' => 'text',
		'default' => 'Tech Support Forum'
	));

	$social_section_options->add_group_field( $community_scroll_items_id, array(
		'name' => 'Community Description',
		'desc' => 'Full Description',
		'id'   => 'community_item_desc',
		'type' => 'textarea',
		'default' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut',
	));

	$social_section_options->add_group_field( $community_scroll_items_id, array(
		'name' => 'Community Item URL',
		'desc' => 'URL of the community item',
		'id'   => 'community_item_url',
		'type' => 'text_url',
		'default' => '#',
	));

	// Press Kits
	$press_id = $social_section_options->add_field( array(
		'id'          => 'press_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'Press Kits',
			'closed'        => true,
		),
	));

	$social_section_options->add_group_field( $press_id, array(
		'name' => 'Card Image',
		'desc' => 'Upload Image',
		'id'   => 'social_press_card_img',
		'type' => 'file',
	));

	$social_section_options->add_group_field( $press_id, array(
		'name' => 'Card Description',
		'desc' => 'Full Description',
		'id'   => 'social_press_card_desc',
		'type' => 'textarea',
	));

	$social_section_options->add_group_field( $press_id, array(
		'name' => 'Press Kit External URL',
		'id'   => 'social_press_url',
		'type' => 'text_url',
	));

	// press popup scroll
	$press_scroll_items_id = $social_section_options->add_field( array(
		'id'          => 'press_popup_scroll_items',
		'type'        => 'group',
		'repeatable'  => true,
		'options'     => array(
			'group_title'   => 'Press Item {#}',
			'add_button'    => 'Add Another Press Item',
			'remove_button' => 'Remove Press Item',
			'closed'        => true,  // Repeater fields closed by default - neat & compact.
			'sortable'      => true,  // Allow changing the order of repeated groups.
		),
	));

	$social_section_options->add_group_field( $press_scroll_items_id, array(
		'name' => 'Press Image',
		'desc' => 'Upload Image',
		'id'   => 'press_item_img',
		'type' => 'file',
	));

	$social_section_options->add_group_field( $press_scroll_items_id, array(
		'name' => 'Press Title',
		'desc' => 'Name of the Press Item',
		'id'   => 'press_item_title',
		'type' => 'text',
		'default'=> 'Game Press Kit'
	));

	$social_section_options->add_group_field( $press_scroll_items_id, array(
		'name' => 'Press Description',
		'desc' => 'Full Description',
		'id'   => 'press_item_desc',
		'type' => 'textarea',
		'default' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut',
	));

	$social_section_options->add_group_field( $press_scroll_items_id, array(
		'name' => 'Press Item URL',
		'desc' => 'URL of the press item',
		'id'   => 'press_item_url',
		'type' => 'text_url',
		'default' => '#',
	));

	/**
	 * =====================================================================
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * 						Contact section
	 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 * =====================================================================
	 */

	$args = array(
		'id'           => $prefix.'contact_options_page',
		'menu_title'   => 'Contact Section', // Use menu title, & not title to hide main h2.
		'object_types' => array( 'options-page' ),
		'option_key'   => $prefix.'contact_section',
		'parent_slug'  => $prefix.'main_options',
		'tab_group'    => $prefix.'main_options',
		'tab_title'    => 'Contact Section',
	);

	// 'tab_group' property is supported in > 2.4.0.
	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
		$args['display_cb'] = $prefix.'options_display_with_tabs';
	}

	$contact_section_options = new_cmb2_box( $args );

	$contact_section_options->add_field( array(
		'name'    => 'Section Header',
		'desc'    => 'Contact section title header',
		'id'      => 'contact_title',
		'type'    => 'text',
		'default' => 'SUMMON US',
	));

	$contact_section_options->add_field( array(
		'name'    => 'Section Description',
		'desc'    => 'Contact section description',
		'id'      => 'contact_desc',
		'type'    => 'textarea',
	));

	$contact_section_options->add_field( array(
		'name'    => 'Email Destination',
		'desc'    => 'Your email address',
		'id'      => 'contact_email',
		'type'    => 'text',
		'default' => 'gan.mahmud@gmail.com',
	));

	// Jordan
	$jordan_id = $contact_section_options->add_field( array(
		'id'          => 'jordan_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'Jordan Location',
			'closed'        => true,
		),
	));

	$contact_section_options->add_group_field( $jordan_id, array(
		'name' => 'Latitude',
		'id'   => 'jordan_lat',
		'type' => 'text',
	));

	$contact_section_options->add_group_field( $jordan_id, array(
		'name' => 'Latitude',
		'id'   => 'jordan_lon',
		'type' => 'text',
	));

	// UAE
	$uae_id = $contact_section_options->add_field( array(
		'id'          => 'uae_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'UAE Location',
			'closed'        => true,
		),
	));

	$contact_section_options->add_group_field( $uae_id, array(
		'name' => 'Latitude',
		'id'   => 'uae_lat',
		'type' => 'text',
	));

	$contact_section_options->add_group_field( $uae_id, array(
		'name' => 'Latitude',
		'id'   => 'uae_lon',
		'type' => 'text',
	));

	// USA
	$usa_id = $contact_section_options->add_field( array(
		'id'          => 'usa_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'USA Location',
			'closed'        => true,
		),
	));

	$contact_section_options->add_group_field( $usa_id, array(
		'name' => 'Latitude',
		'id'   => 'usa_lat',
		'type' => 'text',
	));

	$contact_section_options->add_group_field( $usa_id, array(
		'name' => 'Latitude',
		'id'   => 'usa_lon',
		'type' => 'text',
	));
	// CHINA
	$china_id = $contact_section_options->add_field( array(
		'id'          => 'china_data',
		'type'        => 'group',
		'repeatable'  => false,
		'options'     => array(
			'group_title'   => 'China Location',
			'closed'        => true,
		),
	));

	$contact_section_options->add_group_field( $china_id, array(
		'name' => 'Latitude',
		'id'   => 'china_lat',
		'type' => 'text',
	));

	$contact_section_options->add_group_field( $china_id, array(
		'name' => 'Latitude',
		'id'   => 'china_lon',
		'type' => 'text',
	));
}