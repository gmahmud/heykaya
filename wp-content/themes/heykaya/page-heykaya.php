<?php
/*
Template Name: HEYKAYA
*/
?>

<?php
/**
 * The template for displaying Heykaya Homepage
 *
 * This is the template that displays the Heykaya site by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Heykaya
 */

get_header();
?>

<body <?php body_class();?> data-spy="scroll" data-target=".navbar-custom">
    <div class="container-fluid">
		
		<?php get_template_part( 'template-parts/content', 'headsection' );?>

		<?php get_template_part( 'template-parts/content', 'divider' );?>

		<?php get_template_part( 'template-parts/content', 'features' );?>

		<?php get_template_part( 'template-parts/content', 'divider' );?>

		<?php get_template_part( 'template-parts/content', 'gallery' );?>

		<?php get_template_part( 'template-parts/content', 'divider' );?>

		<?php get_template_part( 'template-parts/content', 'store' );?>

		<?php get_template_part( 'template-parts/content', 'divider' );?>

		<?php get_template_part( 'template-parts/content', 'team' );?>

		<?php get_template_part( 'template-parts/content', 'divider' );?>

		<?php get_template_part( 'template-parts/content', 'community' );?>

		<?php get_template_part( 'template-parts/content', 'divider' );?>

		<?php get_template_part( 'template-parts/content', 'contact' );?>

		<?php get_template_part( 'template-parts/content', 'divider' );?>

		<?php get_template_part( 'template-parts/content', 'footersection' );?>

	</div>
<?php get_footer(); ?>
</body>
</html>