<?php
/**
 * Heykaya functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Heykaya
 */

// =========================================================================
// REMOVE JUNK FROM WORDPRESS HEAD STARTS
// =========================================================================
// REMOVE WORDPRESS HEADER META:
remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
remove_action('wp_head', 'wp_generator'); // remove wordpress version meta tag
remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links
remove_action('wp_head', 'index_rel_link'); // remove link to index page
remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)
remove_action('wp_head', 'start_post_rel_link', 10); // remove random post link
remove_action('wp_head', 'parent_post_rel_link', 10); // remove parent post link
remove_action('wp_head', 'adjacent_posts_rel_link', 10); // remove the next and previous post links
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10); // remove the next and previous post links
remove_action('wp_head', 'wp_shortlink_wp_head', 10); // remove wp shortlink 
// REMOVE EMOJIS:
remove_action('wp_head', 'print_emoji_detection_script', 7); // Emoji Scripts and Styles
remove_action('admin_print_scripts', 'print_emoji_detection_script'); // Emoji Scripts and Styles
remove_action('wp_print_styles', 'print_emoji_styles'); // Emoji Scripts and Styles
remove_action('admin_print_styles', 'print_emoji_styles'); // Emoji Scripts and Styles
add_filter('emoji_svg_url', '__return_false'); // Removes the dns-prefetch meta for Emoji (s.w.org)
// REMOVE REST API:
remove_action('wp_head', 'rest_output_link_wp_head', 10); // Remove the REST API lines from the HTML Header (api.w.org)
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10); // Remove the REST API lines from the HTML Header (api.w.org)
remove_action('rest_api_init', 'wp_oembed_register_route'); // Remove the REST API endpoint.
add_filter( 'embed_oembed_discover', '__return_false'); // Turn off oEmbed auto discovery.
remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10); // Don't filter oEmbed results.
remove_action('wp_head', 'wp_oembed_add_discovery_links'); // Remove oEmbed discovery links.
remove_action('wp_head', 'wp_oembed_add_host_js'); // Remove oEmbed-specific JavaScript from the front-end and back-end.

// =========================================================================
// REMOVE JUNK FROM WORDPRESS HEAD ENDS
// =========================================================================

if ( ! function_exists( 'heykaya_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function heykaya_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Heykaya, use a find and replace
		 * to change 'heykaya' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'heykaya', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'heykaya' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'heykaya_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'heykaya_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function heykaya_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'heykaya_content_width', 640 );
}
add_action( 'after_setup_theme', 'heykaya_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function heykaya_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'heykaya' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'heykaya' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'heykaya_widgets_init' );

// Character Limiting function
function limitchar($text, $maxchar, $end='...') {
    if (strlen($text) > $maxchar || $text == '') {
        $words = preg_split('/\s/', $text);      
        $output = '';
        $i      = 0;
        while (1) {
            $length = strlen($output)+strlen($words[$i]);
            if ($length > $maxchar) {
                break;
            } 
            else {
                $output .= " " . $words[$i];
                ++$i;
            }
        }
        $output .= $end;
    } 
    else {
        $output = $text;
    }
    return $output;
}
// echo limitchar($string,30);
function getYouTubeIdFromURL($url){
  $url_string = parse_url($url, PHP_URL_QUERY);
  parse_str($url_string, $args);
  return isset($args['v']) ? $args['v'] : false;
}
/**
 * Enqueue scripts and styles.
 */
function heykaya_scripts() {
	
	wp_enqueue_style( 'heykaya-google-font', 'https://fonts.googleapis.com/css?family=Cinzel|Cinzel+Decorative', false );
	
	wp_enqueue_style( 'heykaya-custom-font', get_template_directory_uri().'/fonts/ionicons/2.0.1/css/ionicons.min.css', false );

	wp_enqueue_style ('bootstrap', get_template_directory_uri().'/css/vendors/bootstrap/bootstrap.min.css');

	wp_enqueue_style ('fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	
	wp_enqueue_style ('main-style', get_template_directory_uri().'/css/styles.css', array('bootstrap')); 

	wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/js/jquery-1.12.4.min.js', array(), '', true );

	wp_enqueue_script( 'map', 'https://maps-api-ssl.google.com/maps/api/js?key=AIzaSyBqGHoKDotAUnCmJJqIDUzdGO2g3xA0tnY&callback=initMap', array('jquery-js'), '',true );

	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap/bootstrap.min.js', array('jquery-js'), '', true );

	wp_enqueue_script( 'jquery-waypoint', get_template_directory_uri() . '/js/waypoints/jquery.waypoints.min.js', array('jquery-js'), '', true );

	wp_enqueue_script( 'customScroll', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js', array('jquery-js'), '', true );

	wp_enqueue_script( 'owl', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery-js'), '', true );

	wp_enqueue_script( 'app-js', get_template_directory_uri() . '/js/app.min.js', array('jquery-js'), '', true );

	wp_enqueue_script( 'jquery-mobile-custom', get_template_directory_uri() . '/js/jquery-mobile-custom/jquery.mobile.custom.min.js', array('jquery-js'), '', true );

	// wp_enqueue_script( 'heykaya-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'heykaya_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * All of the custom fields are coded here
 */
require get_template_directory() . '/inc/heykaya-all-custom-fields.php';

/**
 * Implementing all the custom fields into the theme settings system
 */
require get_template_directory() . '/inc/heykaya-theme-options.php';

add_action( 'wp_ajax_send_email', 'callback_send_email' );
add_action( 'wp_ajax_nopriv_send_email', 'callback_send_email' );

function callback_send_email(){

  $name = $_REQUEST['name'];
  $email = $_REQUEST['email'];
  $reason = $_REQUEST['reason'];
  $found = $_REQUEST['found'];
  $newsletter = $_REQUEST['newsletter'];
  $message= $_REQUEST['message'];
  $target_email= $_REQUEST['target_email'];
  $subject = "Heykaya Info";
  $email_body = "The following prospectus has contacted you.<br>".
	"Name: $name. <br>".
	"Email: $email. <br>".
	"Reason: $reason. <br>".
	"Found us by: $found. <br>".
	"Newsletter: $newsletter. <br>".
	"Message: $message. <br>";
      $to = $target_email;
      $headers  = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
      $headers .= "From: $name <$email> \r\n";
      $headers .= "Reply-To: $email \r\n";
  $mail = mail($to,$subject,$email_body,$headers);
	if($mail){
      echo "Email Sent Successfully";
    }
}