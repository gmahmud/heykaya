<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Heykaya
 */

?>
<?php echo do_shortcode('[gtranslate]'); ?>
<?php
$country = strtolower(do_shortcode('[geoip_detect2 property="country.isoCode"]')); 
$arab_list = ['dz','td','td','dj','eg','er','ly','mr','ma','sd','tn','tz','bh','iq','jo','kw','lb','om','ps','qa','sa','sy','ae','ye'];
if (in_array($country, $arab_list)){
	$country = "ar";
}
elseif ($country=="cn"){
 	$country = "zh-CN";
}
elseif ($country=="jp"){
 	$country = "ja";
}
elseif (($country=="kp") || ($country=="kr")){
 	$country = "ko";
}
else{
	$country = "en";
} 
?>
<script type="text/javascript">
    window.onload = doGTranslate('en|<?php echo $country; ?>');jQuery('div.switcher div.selected a').html(jQuery(this).html()); 
</script>

<?php wp_footer(); ?>

