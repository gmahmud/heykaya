<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Heykaya
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="Enrich your precious game by complimenting it with the website it truly deserves. Promote your game's iconic characters, deep & sophisticated gameplay, complex strategies, engulfing quests with rich NPCs and malicious foes, with the most visual way possible through Dragonic's unique design. Thriving with small details, this premium template experience will leave your players in a jaw-dropping awe.">
    <meta name="keywords" content="game, premium template, gaming, online gaming, pc game, android, ios, google play store, apple appstore, one page template, dragonic, dragons, themeforest">
	<title>Heykaya Interactive Inc. | Indulge in Immersive and Imaginative Realities!</title>

	<?php wp_head(); ?>
</head>
