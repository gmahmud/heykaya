<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>
<?php
    $prefix = "heykaya_";
    $section = "gallery";
    $title = cmb2_get_option( $prefix.$section.'_section', $section.'_title' );
    $desc = cmb2_get_option( $prefix.$section.'_section', $section.'_desc' );
    $gallery_data = cmb2_get_option( $prefix.$section.'_section', 'gallery_item_group' );
?>
 <!-- ============== GALLERY SECTION =============== -->
<div class="row pattern-dark">
    <section class="gallery" id="multimedia">
        <div class="container">
            <div class="row">
                <h2><?php echo $title; ?></h2>
                <p class="section-description">
                    <?php echo $desc; ?>
                </p>
            </div>
            
        </div>
        <div class="row">
            <div id="dragonic-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="modal-corner modal-corner-top-left"></div>
                <div class="modal-corner modal-corner-top-right"></div>
                <div class="modal-corner modal-corner-bottom-left"></div>
                <div class="modal-corner modal-corner-bottom-right"></div>
                <div class="modal-border modal-border-top"></div>
                <div class="modal-border modal-border-right"></div>
                <div class="modal-border modal-border-bottom"></div>
                <div class="modal-border modal-border-left"></div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php $item_counter=0; ?>
                    <?php foreach ($gallery_data as $item): ?>
                    <li data-target="#dragonic-carousel" data-slide-to="<?php echo $item_counter; ?>" class="<?php echo $item["active_status"]; ?>"></li>
                    <?php $item_counter++; ?>
                    <?php endforeach; ?>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php foreach ($gallery_data as $gallery_item) : ?>
                       <?php if($gallery_item["vid_yes"] == "yes"): ?>
                            
                            <div class="item <?php echo $gallery_item["active_status"]; ?>">
                                <div class="youtube-player" data-id="<?php echo getYouTubeIdFromURL($gallery_item["gallery_slider_vid"]); ?>">
                                    <img src="<?php echo $gallery_item["gallery_slider_img"]; ?>" alt="Dragonic Game Video Gameplay Graphics Showcase">
                                </div>
                            </div>
                            <?php else: ?>

                                <div class="item <?php echo $gallery_item["active_status"]; ?>">
                                    <img src="<?php echo $gallery_item["gallery_slider_img"]; ?>" alt="Dragonic Game Gameplay Screenshot Dragons Mountains">
                                    <div class="carousel-caption">
                                        <?php echo $gallery_item["gallery_slider_caption"]; ?>
                                    </div>
                                </div>
                        <?php endif; ?>
                    
                    <?php endforeach; ?>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#dragonic-carousel" role="button" data-slide="prev">
                <span class="slider-arrow slider-arrow-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#dragonic-carousel" role="button" data-slide="next">
                <span class="slider-arrow slider-arrow-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </section>
</div>