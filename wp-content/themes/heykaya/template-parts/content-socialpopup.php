<?php
    $prefix = "heykaya_";
    $section = "social";

    $title = cmb2_get_option( $prefix.$section.'_section', $section.'_title' );
    $desc = cmb2_get_option( $prefix.$section.'_section', $section.'_desc' );

    $blog_data = cmb2_get_option( $prefix.$section.'_section', 'blog_data' );
    $community_data = cmb2_get_option( $prefix.$section.'_section', 'community_data' );
    $press_data = cmb2_get_option( $prefix.$section.'_section', 'press_data' );

    $blog_scroll_data = cmb2_get_option( $prefix.$section.'_section', 'blog_popup_scroll_items' );
    $community_scroll_data = cmb2_get_option( $prefix.$section.'_section', 'community_popup_scroll_items' );
    $press_scroll_data = cmb2_get_option( $prefix.$section.'_section', 'press_popup_scroll_items' );
?>
<!-- Blog -->
<div class="modal store-popup fade" id="blog-social-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog spop" role="document">
        <div class="modal-content pattern spop">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header spop">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Development Blog</h4>
                <p class="features-p spop">
                  <?php echo $blog_data[0]["social_blog_card_desc"]; ?>
                </p>
            </div>
            <div class="modal-body spop custom clearfix" id="modalBody">
               <div class="col-sm-5 left-img">
                   <div class="img-div" style="background-image: url(<?php echo $blog_data[0]["social_blog_card_img"]; ?>);">
                       
                   </div>
               </div>
               
               <div class="col-sm-7 right-scroll" id="right-scroll">
                   
                  <?php foreach ($blog_scroll_data as $blog_item): ?>
                  
                   <ul class="scroll-items">
                       <li class="scroll-items-left">
                           <div style='background-image: url(<?php echo $blog_item["blog_item_img"]; ?>);'></div>
                       </li>
                       <li class="scroll-items-right">
                        
                           <ul>
                               <li>
                                    <h5><?php echo strtoupper($blog_item["blog_item_title"]); ?></h5>
                                </li>
                               <li>
                                    <p class="features-p"><?php echo limitchar($blog_item["blog_item_desc"],216); ?> </p>
                                </li>
                               <li>
                                    <a target="_blank" href="<?php echo $blog_item['blog_item_url'] ?>" class="button-game shop-modal-hide ns">
                                        <span class="button-game-bg-left"></span>
                                        <span class="button-game-bg-mid spop">
                                        <span>Read More</span>
                                        </span>
                                        <span class="button-game-bg-right"></span>
                                    </a>
                               </li>
                           </ul>
                       </li>
                   </ul>
                  
                  <?php endforeach; ?>
               </div>
            </div>
            <div class="mq" style="display:none;margin: 30px 0;">
              <?php get_template_part( 'template-parts/content', 'divider' );?>     
            </div>
            <div class="modal-footer spop">
                <!--<button type="button" class="btn">Order now</button>-->
                <a target="_blank" href='$blog_data[0]["social_blog_url"];' class="button-game shop-modal-hide ns" href="#store">
                <span class="button-game-bg-left"></span>
                <span class="button-game-bg-mid">
                <span>Visit Heykaya Dev Blog</span>
                </span>
                <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Blog enfds -->
<!-- Community -->
<div class="modal social-popup fade" id="community-social-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog spop" role="document">
        <div class="modal-content pattern spop">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header spop">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Community</h4>
                <p class="features-p spop">
                  <?php echo $community_data[0]["social_community_card_desc"]; ?>
                </p>
            </div>
            <div class="modal-body spop custom clearfix" id="modalBody">
               <div class="col-sm-5 left-img">
                   <div class="img-div" style="background-image: url(<?php echo $community_data[0]["social_community_card_img"]; ?>);">
                       
                   </div>
               </div>
               
               <div class="col-sm-7 right-scroll" id="right-scroll">
                   
                  <?php foreach ($community_scroll_data as $community_item): ?>
                  
                   <ul class="scroll-items bgap">
                       <li class="scroll-items-left">
                           <div class="three" style='background-image: url(<?php echo $community_item["community_item_img"]; ?>);'></div>
                       </li>
                       <li class="scroll-items-right">
                        
                           <ul>
                               <li>
                                    <h5><?php echo strtoupper($community_item["community_item_title"]); ?></h5>
                                </li>
                               <li>
                                    <p class="features-p"><?php echo limitchar($community_item["community_item_desc"],216); ?> </p>
                                </li>
                               <li>
                                    <a target="_blank" href="<?php echo $community_item['community_item_url'] ?>" class="button-game shop-modal-hide ns">
                                        <span class="button-game-bg-left"></span>
                                        <span class="button-game-bg-mid spop">
                                        <span>Visit Forum</span>
                                        </span>
                                        <span class="button-game-bg-right"></span>
                                    </a>
                               </li>
                           </ul>
                       </li>
                   </ul>
                  
                  <?php endforeach; ?>
               </div>
            </div>
            <div class="mq" style="display:none;margin: 30px 0;">
              <?php get_template_part( 'template-parts/content', 'divider' );?>     
            </div>
            <div class="modal-footer spop">
                <!--<button type="button" class="btn">Order now</button>-->
                <a target="_blank" href='$community_data[0]["social_community_url"];' class="button-game shop-modal-hide ns" href="#store">
                <span class="button-game-bg-left"></span>
                <span class="button-game-bg-mid">
                <span>Visit Heykaya Community</span>
                </span>
                <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Community enfds -->
<!-- Press -->
<div class="modal store-popup fade" id="press-social-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog spop" role="document">
        <div class="modal-content pattern spop">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header spop">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Press Kits</h4>
                <p class="features-p spop">
                  <?php echo $press_data[0]["social_press_card_desc"]; ?>
                </p>
            </div>
            <div class="modal-body spop custom clearfix" id="modalBody">
               <div class="col-sm-5 left-img">
                   <div class="img-div" style="background-image: url(<?php echo $press_data[0]["social_press_card_img"]; ?>);">
                       
                   </div>
               </div>
               
               <div class="col-sm-7 right-scroll" id="right-scroll">
                   
                  <?php foreach ($press_scroll_data as $press_item): ?>
                  
                   <ul class="scroll-items">
                       <li class="scroll-items-left">
                           <div style='background-image: url(<?php echo $press_item["press_item_img"]; ?>);'></div>
                       </li>
                       <li class="scroll-items-right">
                        
                           <ul>
                               <li>
                                    <h5><?php echo strtoupper($press_item["press_item_title"]); ?></h5>
                                </li>
                               <li>
                                    <p class="features-p"><?php echo limitchar($press_item["press_item_desc"],216); ?> </p>
                                </li>
                               <li>
                                    <a target="_blank" href="<?php echo $press_item['press_item_url'] ?>" class="button-game shop-modal-hide ns">
                                        <span class="button-game-bg-left"></span>
                                        <span class="button-game-bg-mid spop">
                                        <span>Read More</span>
                                        </span>
                                        <span class="button-game-bg-right"></span>
                                    </a>
                               </li>
                           </ul>
                       </li>
                   </ul>
                  
                  <?php endforeach; ?>
               </div>
            </div>
            <div class="mq" style="display:none;margin: 30px 0;">
              <?php get_template_part( 'template-parts/content', 'divider' );?>     
            </div>
            <div class="modal-footer spop">
                <!--<button type="button" class="btn">Order now</button>-->
                <a target="_blank" href='$press_data[0]["social_press_url"];' class="button-game shop-modal-hide ns" href="#store">
                <span class="button-game-bg-left"></span>
                <span class="button-game-bg-mid">
                <span>Visit Heykaya Press Kits</span>
                </span>
                <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Press enfds -->
