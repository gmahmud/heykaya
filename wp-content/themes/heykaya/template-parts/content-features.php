<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>
<?php
    $prefix = "heykaya_";
    $section = "features";
    $title = cmb2_get_option( $prefix.$section.'_section', 'features_title' );
    $title2 = cmb2_get_option( $prefix.$section.'_section', 'features_title2' );
    $desc = cmb2_get_option( $prefix.$section.'_section', 'features_desc' );
    $game_data = cmb2_get_option( $prefix.$section.'_section', 'game_group' );

?>
<!-- ============== FEATURES SECTION =============== -->

<div class="row pattern">
    <section class="features">
        <div class="container">
            <div class="row">
                <h2><?php echo $title; ?></h2>
                <?php //var_dump(cmb2_get_option( 'heykaya_main_options', 'blog_group' )); ?>
                <p class="section-description">
                    <?php echo $desc; ?>
                </p>
            </div>
            <div class="row items-container bottom-wrapper">
                <p>&nbsp;<br>&nbsp;</p>
            </div>
            <div class="row" id="games">
                <h2><?php echo $title2; ?></h2>
                <div id="game-carousel" class="owl-carousel">
                    <?php foreach ($game_data as $game_item): ?>

                        <?php
                            $game_title = ($game_item["progress_status"] == "in_progress" ? "Work In Progress" : $game_item["game_title"]);
                            $modal_name = strtolower(preg_replace('/[^a-zA-Z0-9_%\[().\]\\/-]/s', '', $game_title))."-modal";
                            $data_tar = "#".$modal_name;
                        ?>
                       
                    <article class="game-item box features-item thumbnail-100"
                    data-toggle='<?php echo ($game_item["progress_status"] == "in_progress" ? "" : "modal"); ?>'
                    data-target='<?php echo ($game_item["progress_status"] == "in_progress" ? "" : $data_tar); ?>'>
                        <img class="features-img" src="<?php echo $game_item["game_thumb"] ?>" alt="Hero Character">
                        
                        
                        <h3><?php echo $game_title; ?></h3>

                        <p class="features-p">
                            <?php echo limitchar($game_item["game_desc"],354); ?> 
                        </p>
                    </article>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
</div>

<?php get_template_part( 'template-parts/content', 'gamepopup' );?>