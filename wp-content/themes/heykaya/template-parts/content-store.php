<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>
<?php
    $prefix = "heykaya_";
    $section = "store";
    $title = cmb2_get_option( $prefix.$section.'_section', $section.'_title' );
    $desc = cmb2_get_option( $prefix.$section.'_section', $section.'_desc' );
    $apple_store_data = cmb2_get_option( $prefix.$section.'_section', 'apple_store_data' );
    $play_store_data = cmb2_get_option( $prefix.$section.'_section', 'play_store_data' );
    $steam_store_data = cmb2_get_option( $prefix.$section.'_section', 'steam_store_data' );
    $heykaya_store_data = cmb2_get_option( $prefix.$section.'_section', 'heykaya_store_data' );
?>
<!-- ============== STORE SECTION =============== -->
<div class="row pattern">
    <section class="store" id="store">
        <div class="container">
            <div class="row">
                <h2><?php echo $title; ?></h2>
                <p class="section-description">
                    <?php echo $desc; ?>
                </p>
            </div>
            <div class="row xs-row-m0">
                <article class="col-sm-6 col-md-3 gallery-item withgap item-1 thumbnail-50">

                    <img data-toggle="modal" data-target="#apple-store-modal" src="<?php echo $apple_store_data[0]["apple_store_card_img"]; ?>" alt="iOS Version">
                    
                    <div data-toggle="modal" data-target="#apple-store-modal" class="panel-item__text withgap">
                        <h3 class="panel-item__title">Apple Store</h3>
                        <p class="panel-item__summary">
                            <?php echo limitchar($apple_store_data[0]["apple_store_card_desc"],75); ?>
                        </p>
                    </div>

                    <a href="<?php echo $apple_store_data[0]["apple_store_url"]; ?>" target="_blank" class="button-game ns">
                        <span class="button-game-bg-left"></span>
                        <span class="button-game-bg-mid">
                        <span>Apple Store&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="ion-social-apple button-game-apple"></span>
                        </span>
                        <span class="button-game-bg-right"></span>
                    </a>
                </article>
                <article class="col-sm-6 col-md-3 gallery-item withgap item-1 thumbnail-50">

                    <img data-toggle="modal" data-target="#play-store-modal" src="<?php echo $play_store_data[0]["play_store_card_img"]; ?>" alt="Android Version">
                   
                    <div data-toggle="modal" data-target="#play-store-modal" class="panel-item__text withgap">
                        <h3 class="panel-item__title">Play Store</h3>
                        <p class="panel-item__summary">
                            <?php echo limitchar($play_store_data[0]["play_store_card_desc"],75); ?>
                        </p>
                    </div>

                    <a href="<?php echo $play_store_data[0]["play_store_url"]; ?>" target="_blank" class="button-game ns">
                        <span class="button-game-bg-left"></span>
                        <span class="button-game-bg-mid">
                        <span>Play Store&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="ion-social-android button-game-android"></span>
                        </span>
                        <span class="button-game-bg-right"></span>
                    </a>
                </article>
                <div class="clearfix visible-sm-block"></div>
                <article class="col-sm-6 col-md-3 gallery-item withgap item-1 thumbnail-50">

                    <img data-toggle="modal" data-target="#steam-store-modal" src="<?php echo $steam_store_data[0]["steam_store_card_img"]; ?>" alt="PC & Mac Version">
                   
                    <div data-toggle="modal" data-target="#steam-store-modal" class="panel-item__text withgap">
                        <h3 class="panel-item__title">Steam Store</h3>
                        <p class="panel-item__summary">
                           <?php echo limitchar($steam_store_data[0]["steam_store_card_desc"],75); ?>
                        </p>
                    </div>

                    <a href="<?php echo $play_store_data[0]["play_store_url"]; ?>" target="_blank" class="button-game ns">
                        <span class="button-game-bg-left"></span>
                        <span class="button-game-bg-mid">
                        <span>Steam Store&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="ion-steam button-game-steam"></span>
                        </span>
                        <span class="button-game-bg-right"></span>
                    </a>
                </article>
                <article class="col-sm-6 col-md-3 gallery-item withgap item-1 thumbnail-50">
                    <img data-toggle="modal" data-target="#heykaya-store-modal" src="<?php echo $heykaya_store_data[0]["heykaya_store_card_img"]; ?>" alt="PC & Mac Version">
                    
                    <div data-toggle="modal" data-target="#heykaya-store-modal" class="panel-item__text withgap">
                        <h3 class="panel-item__title">Heykaya Store</h3>
                        <p class="panel-item__summary">
                            <?php echo limitchar($heykaya_store_data[0]["heykaya_store_card_desc"],75); ?>
                        </p>
                    </div>
                    <a href="<?php echo $heykaya_store_data[0]["heykaya_store_url"]; ?>" target="_blank" class="button-game">
                        <span class="button-game-bg-left"></span>
                        <span class="button-game-bg-mid">
                        <span>Heykaya Store&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="ion-ios-cart button-game-steam"></span>
                        </span>
                        <span class="button-game-bg-right"></span>
                    </a>
                </article>
                <?php get_template_part( 'template-parts/content', 'storepopup' );?>
                
            </div>
        </div>
    </section>
</div>