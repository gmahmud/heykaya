<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>
<?php
    $prefix = "heykaya_";
    $section = "features";
    $game_data = cmb2_get_option( $prefix.$section.'_section', 'game_group' );
?>
<!-- Modal -->
<?php foreach ($game_data as $game_item): ?>
<?php if($game_item["progress_status"] == "complete"): ?>
<?php $modal_name = strtolower(preg_replace('/[^a-zA-Z0-9_%\[().\]\\/-]/s', '', $game_item["game_title"]))."-modal"; ?>
<div class="modal gp fade" id="<?php echo $modal_name; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog game" role="document">
        <div class="modal-content">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header pattern">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo strtoupper($game_item["game_title"]); ?></h4>
            </div>
            <div class="modal-body custom pattern-dark clearfix" id="modalBody">
               <div class="game-banner" style='background-image: url(<?php echo $game_item["game_popup_art"]; ?>);'>
                  <div class="col-md-2 col-xs-12 image-pack" style="height: 160px;position: relative;">
                    <div class="pp" style='background-image: url(<?php echo $game_item["game_thumb"]; ?>);'></div>
                  </div>
                  <div class="col-md-7 col-xs-12 info-pack">
                    <ul class="banner-group img-info">
                       <li><a target="_blank" class="ns" href='<?php echo $game_item["game_popup_url"]; ?>'><i class="fa fa-link"></i><span><?php echo preg_replace('#^https?://#', '', rtrim($game_item["game_popup_url"],'/')); ?></span></a></li>
                       <li><a target="_blank" class="ns" href='<?php echo $game_item["game_popup_support"]; ?>'><i class="fa fa-cog"></i><span>Support</span></a></li>
                   </ul>
                  </div>
                  <div class="col-md-3 col-xs-12 social-pack">
                     <ul class="banner-group social">
                         <li><a target="_blank" class="ns" href="<?php echo $game_item["game_popup_fb"] ?>"><i class="fa fa-facebook"></i></a></li>
                         <li><a target="_blank" class="ns" href="<?php echo $game_item["game_popup_twitter"] ?>"><i class="fa fa-twitter"></i></a></li>
                         <li><a target="_blank" class="ns" href="<?php echo $game_item["game_popup_insta"] ?>"><i class="fa fa-instagram"></i></a></li>
                         <li><a target="_blank" class="ns" href="<?php echo $game_item["game_popup_youtube"] ?>"><i class="fa fa-youtube"></i></a></li>
                         <li><a target="_blank" class="ns" href="<?php echo $game_item["game_popup_twitch"] ?>"><i class="fa fa-twitch"></i></a></li>
                     </ul>
                  </div>
                   
                  
               </div>

               <div class="game-content row">
                  <div class="col-md-5">
                    <div id="gpop-carousel" class="owl-carousel">
                      <?php foreach ($game_item["game_popup_slider_images"] as $slider_image): ?>
                      <div class="game-item-image">
                        <img src="<?php echo $slider_image; ?>">
                      </div>
                      <?php endforeach; ?>

                      <?php if(count($game_item["game_popup_slider_video"])>0): ?>
                        <?php foreach ($game_item["game_popup_slider_video"] as $slider_video): ?>
                        <div class="game-item-image">
                          <img style="display: none;" src="<?php echo get_template_directory_uri(); ?>/img/slider-video-thumb.png">
                          <iframe class="popup-video" style="width:100%;height: 100%;display: none;" src="https://www.youtube-nocookie.com/embed/<?php echo getYouTubeIdFromURL($slider_video); ?>?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                      <?php endforeach; ?>
                      <?php endif; ?>
                    </div>
                  </div>
                  <div class="col-md-7">
                       <p class="features-p game">
                        <?php echo $game_item["game_desc"]; ?>  
                       </p>
                  </div>
               </div>
            </div>
            <div class="modal-footer pattern">
                <!--<button type="button" class="btn">Order now</button>-->
                <a target="_blank" href="<?php echo $game_item["game_popup_url"]; ?>" class="button-game shop-modal-hide ns" href="#store">
                  <span class="button-game-bg-left"></span>
                  <span class="button-game-bg-mid">
                  <span>Download & Play Now</span>
                  </span>
                  <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php endforeach; ?>