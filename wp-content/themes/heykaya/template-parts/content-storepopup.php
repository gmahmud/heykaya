<?php
    $prefix = "heykaya_";
    $section = "store";
    $title = cmb2_get_option( $prefix.$section.'_section', $section.'_title' );
    $desc = cmb2_get_option( $prefix.$section.'_section', $section.'_desc' );
    $apple_store_data = cmb2_get_option( $prefix.$section.'_section', 'apple_store_data' );
    $play_store_data = cmb2_get_option( $prefix.$section.'_section', 'play_store_data' );
    $steam_store_data = cmb2_get_option( $prefix.$section.'_section', 'steam_store_data' );
    $heykaya_store_data = cmb2_get_option( $prefix.$section.'_section', 'heykaya_store_data' );
    $heykaya_store_goods_data = cmb2_get_option( $prefix.$section.'_section', 'heykaya_store_popup_scroll_items' );
?>
<!-- Apple -->
<div class="modal store-popup fade" id="apple-store-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog spop" role="document">
        <div class="modal-content pattern spop">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header spop">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Apple Store</h4>
                <p class="features-p spop">
                  <?php echo $apple_store_data[0]["apple_store_card_desc"]; ?>
                </p>
            </div>
            <div class="modal-body spop custom clearfix" id="modalBody">
               <div class="col-sm-5 left-img">
                   <div class="img-div" style="background-image: url(<?php echo $apple_store_data[0]["apple_store_card_img"]; ?>);">
                       
                   </div>
               </div>
               <?php
                  $prefix = "heykaya_";
                  $section = "features";
                  $game_data = cmb2_get_option( $prefix.$section.'_section', 'game_group' );

                ?>
               <div class="col-sm-7 right-scroll" id="right-scroll">
                   
                  <?php foreach ($game_data as $game_item): ?>
                  
                   <ul class="scroll-items">
                       <li class="scroll-items-left">
                           <div style="background-image: url(<?php echo $game_item["game_thumb"] ?>);"></div>
                       </li>
                       <li class="scroll-items-right">
                          <?php
                            if ( (filter_var($game_item["game_store_url_apple"], FILTER_VALIDATE_URL)) &&  ($game_item["progress_status"] == "complete")){
                              $game_title = $game_item["game_title"];
                              $game_link = $game_item["game_store_url_apple"];
                            }
                            else{
                              $game_title = "Work In Progress";
                              $game_link =  "javascript:void(0);";
                            }
                          ?>
                          
                           <ul>
                               <li>
                                    <h5><?php echo strtoupper($game_title); ?></h5>
                                </li>
                               <li>
                                    <p class="features-p"><?php echo limitchar($game_item["game_desc"],216); ?> </p>
                                </li>
                               <li>
                                    <a target="_blank" href="<?php echo $game_link; ?>" class="button-game shop-modal-hide ns">
                                        <span class="button-game-bg-left"></span>
                                        <span class="button-game-bg-mid spop">
                                        <span>CHECK GAME APP</span>
                                        </span>
                                        <span class="button-game-bg-right"></span>
                                    </a>
                               </li>
                           </ul>
                       </li>
                   </ul>
                  
                  <?php endforeach; ?>
               </div>
            </div>
            <div class="mq" style="display:none;margin: 30px 0;">
              <?php get_template_part( 'template-parts/content', 'divider' );?>     
            </div>
            <div class="modal-footer spop">
                <!--<button type="button" class="btn">Order now</button>-->
                <a target="_blank" href="<?php echo $apple_store_data[0]["apple_store_url"]; ?>" class="button-game shop-modal-hide ns" href="#store">
                <span class="button-game-bg-left"></span>
                <span class="button-game-bg-mid">
                <span>Visit Heykaya Apple Page</span>
                </span>
                <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Apple enfds -->

<!-- Play -->
<div class="modal store-popup fade" id="play-store-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog spop" role="document">
        <div class="modal-content pattern spop">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header spop">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Play Store</h4>
                <p class="features-p spop">
                  <?php echo $play_store_data[0]["play_store_card_desc"]; ?>
                </p>
            </div>
            <div class="modal-body spop custom clearfix" id="modalBody">
               <div class="col-sm-5 left-img">
                   <div class="img-div" style="background-image: url(<?php echo $play_store_data[0]["play_store_card_img"]; ?>);">
                       
                   </div>
               </div>
               <?php
                  $prefix = "heykaya_";
                  $section = "features";
                  $game_data = cmb2_get_option( $prefix.$section.'_section', 'game_group' );

                ?>
               <div class="col-sm-7 right-scroll" id="right-scroll">
                   
                  <?php foreach ($game_data as $game_item): ?>
                  
                   <ul class="scroll-items">
                       <li class="scroll-items-left">
                           <div style="background-image: url(<?php echo $game_item["game_thumb"] ?>);"></div>
                       </li>
                       <li class="scroll-items-right">
                          <?php
                            if ( (filter_var($game_item["game_store_url_play"], FILTER_VALIDATE_URL)) &&  ($game_item["progress_status"] == "complete")){
                              $game_title = $game_item["game_title"];
                              $game_link = $game_item["game_store_url_play"];
                            }
                            else{
                              $game_title = "Work In Progress";
                              $game_link =  "javascript:void(0);";
                            }
                          ?>
                          
                           <ul>
                               <li>
                                    <h5><?php echo strtoupper($game_title); ?></h5>
                                </li>
                               <li>
                                    <p class="features-p"><?php echo limitchar($game_item["game_desc"],216); ?> </p>
                                </li>
                               <li>
                                    <a target="_blank" href="<?php echo $game_link; ?>" class="button-game shop-modal-hide ns">
                                        <span class="button-game-bg-left"></span>
                                        <span class="button-game-bg-mid spop">
                                        <span>CHECK GAME APP</span>
                                        </span>
                                        <span class="button-game-bg-right"></span>
                                    </a>
                               </li>
                           </ul>
                       </li>
                   </ul>
                  
                  <?php endforeach; ?>
               </div>
            </div>
            <div class="mq" style="display:none;margin: 30px 0;">
              <?php get_template_part( 'template-parts/content', 'divider' );?>     
            </div>
            <div class="modal-footer spop">
                <!--<button type="button" class="btn">Order now</button>-->
                <a target="_blank" href="<?php echo $play_store_data[0]["play_store_url"]; ?>" class="button-game shop-modal-hide ns" href="#store">
                <span class="button-game-bg-left"></span>
                <span class="button-game-bg-mid">
                <span>Visit Heykaya Play Page</span>
                </span>
                <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Play ends -->

<!-- Steam starts -->
<div class="modal store-popup fade" id="steam-store-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog spop" role="document">
        <div class="modal-content pattern spop">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header spop">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Steam Store</h4>
                <p class="features-p spop">
                  <?php echo $steam_store_data[0]["steam_store_card_desc"]; ?>
                </p>
            </div>
            <div class="modal-body spop custom clearfix" id="modalBody">
               <div class="col-sm-5 left-img">
                   <div class="img-div" style="background-image: url(<?php echo $steam_store_data[0]["steam_store_card_img"]; ?>);">
                       
                   </div>
               </div>
               <?php
                  $prefix = "heykaya_";
                  $section = "features";
                  $game_data = cmb2_get_option( $prefix.$section.'_section', 'game_group' );

                ?>
               <div class="col-sm-7 right-scroll" id="right-scroll">
                   
                  <?php foreach ($game_data as $game_item): ?>
                  
                   <ul class="scroll-items">
                       <li class="scroll-items-left">
                           <div style="background-image: url(<?php echo $game_item["game_thumb"] ?>);"></div>
                       </li>
                       <li class="scroll-items-right">
                          <?php
                            if ( (filter_var($game_item["game_store_url_steam"], FILTER_VALIDATE_URL)) &&  ($game_item["progress_status"] == "complete")){
                              $game_title = $game_item["game_title"];
                              $game_link = $game_item["game_store_url_play"];
                            }
                            else{
                              $game_title = "Work In Progress";
                              $game_link =  "javascript:void(0);";
                            }
                          ?>
                          
                           <ul>
                               <li>
                                    <h5><?php echo strtoupper($game_title); ?></h5>
                                </li>
                               <li>
                                    <p class="features-p"><?php echo limitchar($game_item["game_desc"],216); ?> </p>
                                </li>
                               <li>
                                    <a target="_blank" href="<?php echo $game_link; ?>" class="button-game shop-modal-hide ns">
                                        <span class="button-game-bg-left"></span>
                                        <span class="button-game-bg-mid spop">
                                        <span>CHECK GAME APP</span>
                                        </span>
                                        <span class="button-game-bg-right"></span>
                                    </a>
                               </li>
                           </ul>
                       </li>
                   </ul>
                  
                  <?php endforeach; ?>
               </div>
            </div>
            <div class="mq" style="display:none;margin: 30px 0;">
              <?php get_template_part( 'template-parts/content', 'divider' );?>     
            </div>
            <div class="modal-footer spop">
                <!--<button type="button" class="btn">Order now</button>-->
                <a target="_blank" href="<?php echo $steam_store_data[0]["steam_store_url"]; ?>" class="button-game shop-modal-hide ns" href="#store">
                <span class="button-game-bg-left"></span>
                <span class="button-game-bg-mid">
                <span>Visit Heykaya Play Page</span>
                </span>
                <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Steam ends -->

<!-- Heykaya Store starts -->
<div class="modal store-popup fade" id="heykaya-store-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog spop" role="document">
        <div class="modal-content pattern spop">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header spop">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Heykaya Store</h4>
                <p class="features-p spop">
                  <?php echo $heykaya_store_data[0]["heykaya_store_card_desc"]; ?>
                </p>
            </div>
            <div class="modal-body spop custom clearfix" id="modalBody">
               <div class="col-sm-5 left-img">
                   <div class="img-div" style="background-image: url(<?php echo $heykaya_store_data[0]["heykaya_store_card_img"]; ?>);">
                       
                   </div>
               </div>
              
               <div class="col-sm-7 right-scroll" id="right-scroll">
                   
                  <?php foreach ($heykaya_store_goods_data as $goods_item): ?>
                  
                   <ul class="scroll-items bgap">
                       <li class="scroll-items-left">
                           <div class="three" style="background-image: url(<?php echo $goods_item["heykaya_store_goods_img"] ?>);"></div>
                       </li>
                       <li class="scroll-items-right">
                          <?php
                            if ( filter_var($goods_item["heykaya_store_goods_url"], FILTER_VALIDATE_URL) ){
                              $goods_title = $goods_item["heykaya_store_goods_title"];
                              $goods_link = $goods_item["heykaya_store_goods_url"];
                            }
                            else{
                              $goods_title = "Work In Progress";
                              $goods_link =  "javascript:void(0);";
                            }
                          ?>
                          
                           <ul>
                               <li>
                                    <h5><?php echo strtoupper($goods_title); ?></h5>
                                </li>
                               <li>
                                    <p class="features-p"><?php echo limitchar($goods_item["heykaya_store_goods_desc"],216); ?> </p>
                                </li>
                               <li>
                                    <a target="_blank" href="<?php echo $goods_link; ?>" class="button-game shop-modal-hide ns">
                                        <span class="button-game-bg-left"></span>
                                        <span class="button-game-bg-mid spop">
                                        <span>CHECK GOODS</span>
                                        </span>
                                        <span class="button-game-bg-right"></span>
                                    </a>
                               </li>
                           </ul>
                       </li>
                   </ul>
                  
                  <?php endforeach; ?>
               </div>
            </div>
            <div class="mq" style="display:none;margin: 30px 0;">
              <?php get_template_part( 'template-parts/content', 'divider' );?>     
            </div>
            <div class="modal-footer spop">
                <!--<button type="button" class="btn">Order now</button>-->
                <a target="_blank" href="<?php echo $steam_store_data[0]["steam_store_url"]; ?>" class="button-game shop-modal-hide ns" href="#store">
                <span class="button-game-bg-left"></span>
                <span class="button-game-bg-mid">
                <span>Visit Heykaya Play Page</span>
                </span>
                <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Heykaya Store ends -->