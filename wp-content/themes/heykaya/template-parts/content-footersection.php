<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>

<!-- ============== FOOTER =============== -->
<div class="row pattern-dark">
    <footer class="container footer">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <nav class="footer-nav">
                    <ul>
                        
                        <li><a href="#games">Games</a></li>
                        <li><a href="#gallery">Multimedia</a></li>
                        <li><a href="#store">Store</a></li>
                        <li><a href="#team">Team</a></li>
                        <li><a href="#social">Social</a></li>
                        <li><a href="#contact">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-xs-12 col-sm-6">
                <nav class="social-nav">
                    <a href="https://facebook.com/heykayainc/" target="_blank"><span class="ion-social-facebook"></span></a>
                    <a href="https://twitter.com/heykayainc" target="_blank"><span class="ion-social-twitter"></span></a>
                    <a href="https://youtube.com/channel/UCaLSak7RkHyPxkqeyR3TlNA/" target="_blank"><span class="ion-social-youtube"></span></a>
                    <a href="https://instagram.com/heykayainc/" target="_blank"><span class="ion-social-instagram"></span></a>
					<a href="https://steamcommunity.com/id/HeykayaInc/" target="_blank"><span class="ion-steam"></span></a>
                    <a href="https://twitch.tv/heykayainc/" target="_blank"><span class="ion-social-twitch"></span></a>
                    <a href="https://reddit.com/r/heykayainc/" target="_blank"><span class="ion-social-reddit"></span></a>
                    <a href="https://snapchat.com/add/heykayainc" target="_blank"><span class="ion-social-snapchat"></span></a>
                </nav>
            </div>
        </div>
        <div class="row">
            <p class="copyright">
   &copy; 2017<script>new Date().getFullYear()>2017&&document.write("-"+new Date().getFullYear());</script>, Heykaya Interactive Inc.</p>
        </div>
    </footer>
</div>