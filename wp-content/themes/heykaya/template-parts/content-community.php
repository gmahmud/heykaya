<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>
<?php
    $prefix = "heykaya_";
    $section = "social";
    $title = cmb2_get_option( $prefix.$section.'_section', $section.'_title' );
    $desc = cmb2_get_option( $prefix.$section.'_section', $section.'_desc' );
    $blog_data = cmb2_get_option( $prefix.$section.'_section', 'blog_data' );
    $community_data = cmb2_get_option( $prefix.$section.'_section', 'community_data' );
    $press_data = cmb2_get_option( $prefix.$section.'_section', 'press_data' );
?>
<!-- ============== COMMUNITY SECTION =============== -->
<div class="row pattern">
    <section class="store" id="social">
        <div class="container">
            <div class="row">
                <h2><?php echo $title; ?></h2>
                <p class="section-description">
                    <?php echo $desc; ?>

                </p>
            </div>
            <div class="row items-container" >
                <div class="col-xs-5 col-sm-3 gallery-item item-1 thumbnail-50">

                    <img data-toggle="modal" data-target="#blog-social-modal" src="<?php echo $blog_data[0]["social_blog_card_img"]; ?>" alt="iOS Version">
                   
                    <div data-toggle="modal" data-target="#blog-social-modal" class="panel-item__text">
                        <h3 class="panel-item__title">Development Blog</h3>
                        <p class="panel-item__summary">
                            <?php echo $blog_data[0]["social_blog_card_desc"]; ?>
                        </p>
                    </div>

                    <a href="<?php echo $blog_data[0]["social_blog_url"]; ?>" target="_blank" class="button-game ns">
                        <span class="button-game-bg-left"></span>
                        <span class="button-game-bg-mid">
                        <span>Dev Blog&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="ion-chatbubbles button-game-apple"></span>
                        </span>
                        <span class="button-game-bg-right"></span>
                    </a>
                </div>
                <div class="col-xs-5 col-xs-offset-2 col-sm-3 col-sm-offset-1 gallery-item item-2 thumbnail-50">
                    <img data-toggle="modal" data-target="#community-social-modal" src="<?php echo $community_data[0]["social_community_card_img"]; ?>" alt="iOS Version">
                    
                    <div data-toggle="modal" data-target="#community-social-modal" class="panel-item__text">
                        <h3 class="panel-item__title">Our Community</h3>
                        <p class="panel-item__summary">
                            <?php echo $community_data[0]["social_community_card_desc"]; ?>
                        </p>
                    </div>
                    <a href="<?php echo $community_data[0]["social_community_url"]; ?>" target="_blank" class="button-game ns">
                        <span class="button-game-bg-left"></span>
                        <span class="button-game-bg-mid">
                        <span>Community&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="ion-ios-people button-game-android"></span>
                        </span>
                        <span class="button-game-bg-right"></span>
                    </a>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-xs-5 col-sm-3 col-sm-offset-1 gallery-item item-3 thumbnail-50">
                    <img data-toggle="modal" data-target="#press-social-modal" src="<?php echo $press_data[0]["social_press_card_img"]; ?>" alt="PC & Mac Version">
                    
                    <div data-toggle="modal" data-target="#press-social-modal" class="panel-item__text">
                        <h3 class="panel-item__title">Press Kits</h3>
                        <p class="panel-item__summary">
                             <?php echo $press_data[0]["social_press_card_desc"]; ?>
                        </p>
                    </div>
                    <a href="<?php echo $press_data[0]["press_url"]; ?>" target="_blank" class="button-game ns">
                        <span class="button-game-bg-left"></span>
                        <span class="button-game-bg-mid">
                        <span>Press Kits&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="ion-social-rss button-game-steam"></span>
                        </span>
                        <span class="button-game-bg-right"></span>
                    </a>
                </div>

                <?php get_template_part( 'template-parts/content', 'socialpopup' );?>
            </div>
        </div>
    </section>
</div>