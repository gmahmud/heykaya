<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>
<?php
    $prefix = "heykaya_";
    $section = "contact";
    $title = cmb2_get_option( $prefix.$section.'_section', 'contact_title' );
    $desc = cmb2_get_option( $prefix.$section.'_section', 'contact_desc' );
    $email = cmb2_get_option( $prefix.$section.'_section', 'contact_email' );
    $jordan = cmb2_get_option( $prefix.$section.'_section', 'jordan_data' );
    $uae = cmb2_get_option( $prefix.$section.'_section', 'uae_data' );
    $usa = cmb2_get_option( $prefix.$section.'_section', 'usa_data' );
    $china = cmb2_get_option( $prefix.$section.'_section', 'china_data' );
?>
 <!-- ============== CONTACT SECTION =============== -->
<div class="row pattern">
    <section class="contact" id="contact">
        <div class="container">
            <div class="row">
                <h2><?php echo $title; ?></h2>
                <p class="section-description">
                   <?php echo $desc; ?>
                </p>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <form id="contact_form" action="#" method="post" class="contact-form">
                        <div class="row">
                            <div class="col-sm-4 label-container">
                                <label for="name">Name: </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" name="name" id="name" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 label-container">
                                <label for="email">Email: </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="email" name="email" id="email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 label-container">
                                <label>Reason: </label>
                            </div>
                            <div class="col-sm-8">
                                <select class="form-control" name="reason" id="reason">
                                    <option selected>Career</option>
                                    <option>Submit Bug</option>
                                    <option>Testimonial</option>
                                    <option>Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 label-container">
                                <label>You found us: </label>
                            </div>
                            <div class="col-sm-8">
                                <select class="form-control" name="found" id="found">
                                    <option selected>Search Engine</option>
                                    <option>Friend</option>
                                    <option>Advertisement</option>
                                    <option>Free Promotion</option>
                                    <option>Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 label-container">
                                <label>Newsletter:</label>
                            </div>
                            <div class="col-sm-8">
                                <label id="checkbox" class="col-sm-12">
                                <input type="checkbox" name="newsletter" value="Of course!" id="newsletter" checked="checked">
                                <span class="choice">
                                <span class="choice-text">Of course!</span>
                                </span>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 label-container">
                                <label>Message:</label>
                            </div>
                            <div class="col-sm-8">
                                <textarea name="message" id="message" placeholder="Your message..."></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 button-submit">
                                <input type="hidden" id="target_email" name="target_email" value="<?php echo $email; ?>" style="display: none;">
                            </div>
                            <div class="col-sm-8">                                
            					<button type="submit" id="sub" class="ns button-game submit" style=" background: transparent; border: none; ">
                                    <span class="button-game-bg-left"></span>
                                    <span class="button-game-bg-mid">
                                    <span>Submit&nbsp;&nbsp;|&nbsp;&nbsp;</span><span class="ion-android-send"></span>
                                    </span> 
                                    <span class="button-game-bg-right"></span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-lg-offset-1 col-lg-7 ">
                    <div id="location-group">
                        <ul>
                            <li>
                                <a id="jordan" title="JORDAN" data-lat="<?php echo ($jordan[0]["jordan_lat"]); ?>" data-lon="<?php echo $jordan[0]["jordan_lon"]; ?>" data-info="Our headquarters and main workshop situated in beautiful Amman!" name="0" onclick="SetMarker(this.dataset.lat,this.dataset.lon,this.dataset.info,this.title)" href="#location" class="map-loc button-game">
                                    <span class="button-game-bg-left"></span>
                                    <span class="button-game-bg-mid">
                                        <span>Jordan</span>
                                    </span>
                                    <span class="button-game-bg-right"></span>
                                </a>
                            </li>
                            <li>
                                <a id="uae" title="UAE" data-lat="<?php echo $uae[0]["uae_lat"]; ?>" data-lon="<?php echo $uae[0]["uae_lon"]; ?>" data-info="Our headquarters and main workshop situated in UAE!" href="#location" name="1" onclick="SetMarker(this.dataset.lat,this.dataset.lon,this.dataset.info,this.title)" class="map-loc button-game">
                                    <span class="button-game-bg-left"></span>
                                    <span class="button-game-bg-mid">
                                        <span>UAE</span>
                                    </span>
                                    <span class="button-game-bg-right"></span>
                                </a>
                            </li>
                            <li>
                                <a id="usa" title="USA" data-lat="<?php echo $usa[0]["usa_lat"]; ?>" data-lon="<?php echo $usa[0]["usa_lon"]; ?>" data-info="Our headquarters and main workshop situated in USA!" name="2" onclick="SetMarker(this.dataset.lat,this.dataset.lon,this.dataset.info,this.title)" href="#location" class="map-loc button-game">
                                    <span class="button-game-bg-left"></span>
                                    <span class="button-game-bg-mid">
                                        <span>USA</span>
                                    </span>
                                    <span class="button-game-bg-right"></span>
                                </a>
                            </li>
                            <li>
                                <a id="china" title="CHINA" data-lat="<?php echo $china[0]["china_lat"]; ?>" data-lon="<?php echo $china[0]["china_lon"]; ?>" data-info="Our headquarters and main workshop situated in China!" name="3" onclick="SetMarker(this.dataset.lat,this.dataset.lon,this.dataset.info,this.title)" href="#location" class="map-loc button-game">
                                    <span class="button-game-bg-left"></span>
                                    <span class="button-game-bg-mid">
                                        <span>China</span>
                                    </span>
                                    <span class="button-game-bg-right"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="gmaps"></div>
                </div>
            </div>
        </div>
    </section>
</div>