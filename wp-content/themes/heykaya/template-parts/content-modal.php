<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>
<!-- Modal -->
<div class="modal fade" id="shop-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <span class="Video-border Video-border--top"></span>
            <span class="Video-border Video-border--right"></span>
            <span class="Video-border Video-border--bottom"></span>
            <span class="Video-border Video-border--left"></span>
            <span class="Video-corner Video-corner--topLeft"></span>
            <span class="Video-corner Video-corner--topRight"></span>
            <span class="Video-corner Video-corner--bottomRight"></span>
            <span class="Video-corner Video-corner--bottomLeft"></span>
            <div class="modal-header pattern">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body custom pattern-dark clearfix" id="modalBody">
               <div class="custom-modal-item">
                    <img src="" id="showcase-img" class="img-thumbnail" alt="Showcase Dragonic Game">
               </div>
               <div class="custom-modal-item">
                    <ul id="model-feats">
                        <li>Example</li>
                        <li>Example</li>
                        <li>Example</li>
                        <li>Example</li>
                        <li>Example</li>
                    </ul>
               </div>
            </div>
            <div class="modal-footer pattern">
                <!--<button type="button" class="btn">Order now</button>-->
                <a xmlns="http://www.w3.org/1999/xhtml" class="button-game shop-modal-hide" href="#store">
                <span class="button-game-bg-left"></span>
                <span class="button-game-bg-mid">
                <span>Download & Play Now</span>
                </span>
                <span class="button-game-bg-right"></span>
                </a>
            </div>
        </div>
    </div>
</div>