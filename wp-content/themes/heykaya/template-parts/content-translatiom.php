<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */

?>
<?php
    $prefix = "heykaya_";
    $bg = cmb2_get_option( $prefix.'main_options', 'hero_image' );
    $logo = cmb2_get_option( $prefix.'main_options', 'big_logo' );
?>
<!-- ============== HEADER =============== -->
<div class="row">
    <header id="home" style="background-image: linear-gradient(rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35)), url(<?php echo $bg; ?>);">
        <!-- Main nav -->
        <nav class="navbar navbar-inverse navbar-custom navbar-fixed-top">
            <div class="container">
                <div class="navbar navbar-header">
                    <a class="navbar-brand navbar-logo" href="#home"></a>
                    <button type="button" class="navbar-toggle custom-toggle-btn" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                    <span class="sr-only">
                    Toggle navigation
                    </span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="main-nav">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="plain-link"><a href="#games">Games</a></li>
                        <li class="plain-link"><a href="#multimedia">Multimedia</a></li>
                        <li class="plain-link"><a href="#store">Store</a></li>
                        <li class="plain-link"><a href="#team">Team</a></li>
                        <li class="plain-link"><a href="#social">Social</a></li>
                        <li class="plain-link"><a href="#contact">Contact</a></li>
                        <li class="lang">
                            <div class="dropdown">
                                <button type="button" class="search-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="ion-earth"></span>
                                </button>
                                <div class="dropdown-menu dropdown-search-box animated fadeIn">
                                   <ul class="languagepicker roundborders">
                          <li>
                                            <a href="#en" onclick="doGTranslate('en|en');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/en.gif"/>English</a>
                                        </li>

                        <li>
                                            <a href="#ar" style="font-family: 'muslimah'; font-size: 18px" onclick="doGTranslate('en|ar');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;">
                                                <img src="<?php echo get_template_directory_uri(); ?>/img/flags/ar.gif"/>الـعـربـيـة
                                            </a>
                                        </li>

                          <li>
                                            <a href="#tr" onclick="doGTranslate('en|tr');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/tr.gif"/>Türk</a>
                                        </li>

                        <li>
                                            <a href="#es" onclick="doGTranslate('en|es');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/sp.gif"/>Español</a>
                                        </li>

                        <li>
                                            <a href="#fr" onclick="doGTranslate('en|fr');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/fr.gif"/>Français</a>
                                        </li>

                        <li>
                                            <a href="#gr" onclick="doGTranslate('en|de');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/gr.gif"/>Deutsch</a>
                                        </li>

                        <li>
                                            <a href="#ch" onclick="doGTranslate('en|zh-CN');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/cn.gif"/>中文</a>
                                        </li>

                        <li>
                                            <a href="#jp" onclick="doGTranslate('en|ja');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/jp.gif"/>日本語</a>
                                        </li>

                        <li>
                                            <a href="#ko" onclick="doGTranslate('en|ko');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;"><img src="<?php echo get_template_directory_uri(); ?>/img/flags/kr.gif"/>한국어</a>
                                        </li>
                      </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <h1>
            <img onload="this.style.opacity='1';" class="big-logo" src="<?php echo $logo ?>" alt="Heykaya Interactive Logo">
        </h1>
    </header>
</div>