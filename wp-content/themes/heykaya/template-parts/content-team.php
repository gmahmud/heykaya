<?php
/**
 * Template part for displaying heykaya homepage content in page-heykaya.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Heykaya
 */
?>
<?php
    $prefix = "heykaya_";
    $section = "team";
    $title = cmb2_get_option( $prefix.$section.'_section', $section.'_title' );
    $desc = cmb2_get_option( $prefix.$section.'_section', $section.'_desc' );
    $team_data = cmb2_get_option( $prefix.$section.'_section', 'team_member_group' );
?>
<!-- ============== Team SECTION =============== -->
<div class="row" id="team">
    <section class="testimonials" id="testimonials">
        <div class="container">
            <div class="row">
                <h2><?php echo $title; ?></h2>
                <p class="section-description">
                    <?php echo $desc; ?>

                </p>
                
            </div>
            <div class="row">
                <div class="owl-carousel" id="team-carousel">
                    <?php foreach ($team_data as $memb) : ?>
                        
                    
                    
                    <section class="team-item">
                        <cite><img src="<?php echo $memb["team_member_image"]; ?>" alt="<?php echo $memb["team_member_name"]; ?>"></cite>
                        <blockquote>
                            <?php echo $memb["team_member_bio"] ?>
                        </blockquote>
                        <p style="line-height: 22px;">
                            <strong class="team-name" style="font-family: Cinzel Decorative, Times, Times New Roman, Georgia, serif !important; font-size: 24px;"><?php echo $memb["team_member_name"]; ?></strong><br><span style="font-size: 14px; color: #55c8d3;"><?php echo $memb["team_member_designation"]; ?></span><br>
                            <div style="margin: 0 auto; width: 150px; text-align: center;">

								<a class="ns" href="<?php echo $memb["team_member_fb"] ?>" target="_blank"><span style=" color: #fff; padding: 0px 5px" class="ion-social-facebook"></span></a>
								<a class="ns" href="<?php echo $memb["team_member_twitter"] ?>" target="_blank"><span style=" color: #fff; padding: 0px 5px" class="ion-social-twitter"></span></a>
								<a class="ns" href="<?php echo $memb["team_member_linkedin"] ?>" target="_blank"><span style=" color: #fff; padding: 0px 5px" class="ion-social-linkedin"></span></a>
                            </div>
                        </p>
                    </section>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
</div>