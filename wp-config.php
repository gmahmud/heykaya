<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'heykaya');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9kjd:P_I]i=]Y:rOV6, kxW|[+q2Dg*JHx~(YK_V<S6#:^Nnp6Xk<YJbu{*:2X}h');
define('SECURE_AUTH_KEY',  'ZA12Jykh?DQp$E|tdrM5#J4&a~52[j^;-y#dgr4w{dqC3V84VhkwR9h5[q]XSIC<');
define('LOGGED_IN_KEY',    '/%$c3W[B cJ&BxDmt&4{>O)L7BSHN5dE JH[h& >zC_n?Akw}/G<o;}9eNK-6g4w');
define('NONCE_KEY',        'O^I1_BMF =Y,s6c74P9%)2zuN^T) M2:1%[S FQh)[QtP*Z=e &C!g4L]qkDJCdp');
define('AUTH_SALT',        ';`VC|LuNI+3.a96Q+%Cv}&Dq1_c$Y[, _j!9!=8IV]V91-%xAr-VR>43ru0>7$2b');
define('SECURE_AUTH_SALT', 'W8lLgs(k[;ffB~Arn)x{zFnvGTCH:[P77D>#U>l(?cWV+&g2?o[MGC3)b/3N&rT[');
define('LOGGED_IN_SALT',   'AKx/LJrc9|Jq0Gn<_Z}B2wew*$bdZw8+p;WoO +CJ6g*mYd;v2Pw$[,9gs);kSPw');
define('NONCE_SALT',       ';<xFpj*.]bo<A=L[H5m`NvKXyLzeAK*BaHjhs4ieG?2(Yk)k7k0vG>=]N/ks?!}W');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'heykaya_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
